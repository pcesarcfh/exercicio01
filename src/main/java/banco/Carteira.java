package banco;

import java.util.ArrayList;
import java.util.List;

public class Carteira {
	String nome;
	List<PessoaFisica> cartpf = new ArrayList<PessoaFisica>();
	List<PessoaJuridica> cartpj = new ArrayList<PessoaJuridica>();
	
	public Carteira(String nome) {
		this.nome = nome;
	}
	
	public boolean abrirConta(PessoaJuridica pj) {
		cartpj.add(pj);
		return true;
	}

	public boolean abrirConta(PessoaFisica pf) {
		cartpf.add(pf);
		return true;
	}
	
	public List<PessoaFisica> getCarteirapf() {
		for (PessoaFisica pf: cartpf) {
			System.out.println("PessoaFisica: " + pf.getNome()  
			+ " CPF: " + pf.getCpf());
		}
		return cartpf;
	}
	public List<PessoaJuridica> getCarteirapj() {
		for (PessoaJuridica pj: cartpj) {
			System.out.println("PessoaJuridica: " + pj.getNome()  
			+ " CNPJ: " + pj.getCnpj());
		}
		return cartpj;
	}
}